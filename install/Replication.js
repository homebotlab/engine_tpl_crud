const fs = require("fs");
const { join } = require("path");
const configurator = require(`${process.env.HELPERS_PATH}/modules`);

const readdir = fs.promises.readdir;
const readFile = fs.promises.readFile;
const writeFile = fs.promises.writeFile;
const lstat = fs.promises.lstat;
const mkdir = fs.promises.mkdir;

const modulePath = join(__dirname, "../");
const dirCoreModule = join(__dirname, "../../");
const dirLoadingModule = join(__dirname, "../../../../app/modules");

const getAllFiles = async (src, fileList) => {
    const files = await readdir(join(modulePath, src));
    fileList = fileList || [];
    for (const file of files) {
        const fileStatus = await lstat(join(modulePath, src, file));
        if (fileStatus.isDirectory()) {
            fileList = await getAllFiles(join(src, file, "/"), fileList);
        } else {
            fileList.push(join(src, file));
        }
    }
    return fileList;
}

module.exports = class Replication {
    
    async copy(newId) {
        const replaceList = [
            {
                regex: new RegExp("engine.tpl_crud", "g"),
                newValue: newId
            },
            {
                regex: new RegExp("engine/tpl_crud", "g"),
                newValue: newId.split(".").join("/")
            },
            {
                regex: new RegExp("tpl_crud", "g"),
                newValue: newId.split(".")[newId.split(".").length - 1]
            }
        ];

        const webappFiles = await getAllFiles("./webapp");
        const graphqlFiles = await getAllFiles("./graphql");
        const routesFiles = await getAllFiles("./routes");
        const modelFiles = await getAllFiles("./models");

        const dirFiles = [
            ...webappFiles,
            ...graphqlFiles,
            ...routesFiles,
            ...modelFiles,
            "manifest.json"
        ];
        
        // if the module exists then save to /app/modules
        const newModulePath = configurator.getModule(newId) 
            ? join(dirLoadingModule, newId)
            : join(dirCoreModule, newId);
        
        for (const file of dirFiles) {
            let fileData = await readFile(join(modulePath, file), "utf8");
            replaceList.forEach(({regex, newValue}) => {
                fileData = fileData.replace(regex, newValue)
            });
            await mkdir(join(newModulePath, file, "../"), { recursive: true });
            await writeFile(join(newModulePath, file), fileData, 'utf-8');
        };
    }
}