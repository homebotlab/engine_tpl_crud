const bodyParser = require('body-parser');
const { Router } = require('express');
const { formatError } = require('apollo-errors');
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');

const schema = require('../graphql');
const router  = Router();

router.use('/graphql', bodyParser.json(), graphqlExpress(req => {
  return {    
    formatError,
    schema, 
    context: {
      user: req.user
    }
  }
}));
router.use('/graphiql', graphiqlExpress({ endpointURL: '/api/v0.1/tpl_crud/graphql' }));

module.exports = router;