module.exports = `
	type Query {
		entities: [Entity]
		entity(_id: ID!): Entity
	}

	type Mutation {
		addEntity(data: AddEntityInput): Entity
		updateEntity(_id: ID!, data: UpdateEntityInput): Entity
		removeEntity(_id: ID!): Entity
	}

	type Entity {
		_id: ID
		userName: String,
		firstName: String,
		lastName: String,
		age: Int,
		owner: Owner
	}
	
	type Owner {
		_id: ID
		firstname: String
		middlename: String
		lastname: String
		email: String
	}

	input AddEntityInput {
		userName: String,
		firstName: String,
		lastName: String,
		age: Int
	}

	input UpdateEntityInput {
		userName: String,
		firstName: String,
		lastName: String,
		age: Int
	}
`;