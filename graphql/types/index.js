const { mergeTypes } = require("merge-graphql-schemas");
const Entity = require("./Entity");

const typeDefs = [Entity];

module.exports = mergeTypes(typeDefs, {
    all: true
});