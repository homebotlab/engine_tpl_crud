const { mergeResolvers } = require("merge-graphql-schemas");
const Entity = require('./Entity');

const resolvers = [Entity];

module.exports = mergeResolvers(resolvers);