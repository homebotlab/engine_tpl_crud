const Entity = require("../../../models/Entity");
const {
	ForbiddenError,
	UnauthorizedError,
	NotFoundError
} = require(`${process.env.HELPERS_PATH}/graphqlError`);
const acl = require(`${process.env.HELPERS_PATH}/acl`);

module.exports = {
  Query: {    
	entities: async (parent, args, ctx) => {
		if (!ctx.user) throw new UnauthorizedError();
		const entities = await Entity.find()
			.populate("owner")
			.exec();

		const role = ctx.user.role;
		const allowedEntities = [];
		entities.forEach(entity => {
			const permission = (ctx.user._id == entity.owner._id)
			? acl().can(role).readOwn('entity')
			: acl().can(role).readAny('entity');
	
			if (permission.granted) {
			  const filteredEntity = permission.filter(JSON.parse(JSON.stringify(entity)));
			  allowedEntities.push(filteredEntity);
			}
		});
		return allowedEntities;
	},

	entity: async (parent, { _id }, ctx) => {
		if (!ctx.user) throw new UnauthorizedError();
		const entity = await Entity.findById(_id)
			.populate("owner")
			.exec();

		const role = ctx.user.role;
		const permission = (ctx.user._id == entity.owner._id)
		  ? acl().can(role).readOwn('entity')
		  : acl().can(role).readAny('entity');
  
		if (permission.granted) {
		  const filteredEntity = permission.filter(JSON.parse(JSON.stringify(entity)));
		  return filteredEntity;
		}
		throw new ForbiddenError();
	}
  },

  Mutation: {

	addEntity: async (parent, { data }, ctx) => {
		if (!ctx.user) throw new UnauthorizedError();

		const role = ctx.user.role;
		const permission = acl().can(role).createOwn('entity');
      	if (permission.granted) {
			const filteredData = permission.filter({ ...data });
			const entityData = {
				...filteredData,
				owner: ctx.user._id
			};
			const entity = new Entity(entityData);
			const newEntity = await entity.save();
			return newEntity;
      	}
      	throw new ForbiddenError();
	},

	updateEntity: async (parent, { _id, data }, ctx) => {
		if (!ctx.user) throw new UnauthorizedError();

		const entity = await Entity.findById(_id)			
			.populate("owner")
			.exec();

		if (!entity) throw new NotFoundError();
		const role = ctx.user.role;
		const permission = (ctx.user._id == entity.owner._id)
		  ? acl().can(role).updateOwn('entity')
		  : acl().can(role).updateAny('entity');  
		if (permission.granted) {
			const filteredData = permission.filter({ ...data });
			const updatedEntity = await Entity.findByIdAndUpdate(_id, filteredData, {
				new: true
			})
			.populate("owner")
			.exec();
				
			return updatedEntity;
		}
		throw new ForbiddenError();
	},

	removeEntity: async (parent, { _id }, ctx) => {
		if (!ctx.user) throw new UnauthorizedError();

		const entity = await Entity.findById(_id).exec();
		if (!entity) throw new NotFoundError(); 

		const role = ctx.user.role;
		const permission = (ctx.user._id == entity.owner._id)
		  ? acl().can(role).deleteOwn('entity')
		  : acl().can(role).deleteAny('entity');  
		if (permission.granted) {
			const removedEntity = Entity.findByIdAndRemove(_id)			
				.populate("owner")
				.exec();
			return removedEntity;
		}
		throw new ForbiddenError();
	}
  }
};