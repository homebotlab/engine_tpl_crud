const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const EntitySchema = new Schema({
    userName: String,
    firstName: String,
    lastName: String,
    age: Number,
    owner: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
});

module.exports = mongoose.model("Entity", EntitySchema);