sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function(JSONModel, Device) {
	"use strict";
	
	const responseFields = `{
		_id
		userName
		firstName
		lastName
		age
		owner {
			_id,
			firstname,
			middlename,
			lastname,
			email
		}
	}`;

	return {

		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		loadObjects: function() {
			return jQuery.ajax({
				url: '/api/v0.1/tpl_crud/graphql',
				method: 'POST',
				contentType: "application/json",
				data: JSON.stringify({
					query: `
				  {
					entities
					${responseFields}
				  }`
				})
		  })
		},

		loadObject: function(id) {
			return jQuery.ajax({
				url: '/api/v0.1/tpl_crud/graphql',
				method: 'POST',
				contentType: "application/json",
				data: JSON.stringify({
					query: `
				  {
					entity (_id: "${id}") 
					${responseFields}
				  }`
				})
		  })
		},

		addEntity: function(oData){
		  	return jQuery.ajax({
				url: '/api/v0.1/tpl_crud/graphql',
				method: 'POST',
				contentType: "application/json",
				data: JSON.stringify({
				query: `
					mutation {
						addEntity(
							data: {
								userName: "${oData.userName || ''}",
								firstName: "${oData.firstName || ''}",
								lastName: "${oData.lastName || ''}",
								age: ${oData.age || ''}
							} 
						)
						${responseFields}
					}
				`
				})
		  	})
		},

		updateEntity: function(oData){
		  	return jQuery.ajax({
				url: '/api/v0.1/tpl_crud/graphql',
				method: 'POST',
				contentType: "application/json",
				data: JSON.stringify({
					query: `
					mutation {
						updateEntity(
							_id: "${oData._id}",
							data: {
								userName: "${oData.userName || ''}",
								firstName: "${oData.firstName || ''}",
								lastName: "${oData.lastName || ''}",
								age: ${oData.age || ''}
							} 
						) 
						${responseFields}
					}
				`
				})
		  	})
		},

		removeEntity: function(id) {
			return jQuery.ajax({
				url: '/api/v0.1/tpl_crud/graphql',
				method: 'POST',
				contentType: "application/json",
				data: JSON.stringify({
					query: `
					mutation {
						removeEntity(
							_id: "${id}",
						)
						${responseFields}
					}
				`
				})
			})
		},

	};
});